package fr.solennprovost.backenddevexam.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Vehicule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String plaque_immatriculation;
    @Column(nullable = false)
    private String marque;
    @Column(nullable = false)
    private String annee_construction;

}
