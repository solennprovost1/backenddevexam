package fr.solennprovost.backenddevexam.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Manager {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String nom;
    @Column(nullable = false)
    private String prenom;
    private String telephone_fixe;
    @Column(nullable = false)
    private String telephone_portable;
    @OneToMany(mappedBy = "manager")
    private List<Technicien> list_technicien;
}
