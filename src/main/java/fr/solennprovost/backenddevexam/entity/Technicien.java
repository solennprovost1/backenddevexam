package fr.solennprovost.backenddevexam.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Technicien {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String nom;
    @Column(nullable = false)
    private String prenom;
    @Column(nullable = false)
    private Date date_de_naissance;
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Adresse adresse;
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Vehicule vehicule;
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Manager manager;
    @ManyToMany(mappedBy = "techniciens")
    private List<Chantier> chantiers;

}
