package fr.solennprovost.backenddevexam.api.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Jacksonized
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
public class ManagerDTO extends ManagerWithoutTechniciens{
    private List<TechnicienMinimalDTO> technicienMinimalDTOList;
}
