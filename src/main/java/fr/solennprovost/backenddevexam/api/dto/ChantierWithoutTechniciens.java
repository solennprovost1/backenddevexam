package fr.solennprovost.backenddevexam.api.dto;

import fr.solennprovost.backenddevexam.entity.Adresse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@Jacksonized
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
public class ChantierWithoutTechniciens extends ChantierMinimalDTO{

    private Adresse adresse;

}
