package fr.solennprovost.backenddevexam.api.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@Jacksonized
@SuperBuilder
@Data
public class ChantierMinimalDTO {

    private Long id;
    private String nom;
    private Long prix;

}
