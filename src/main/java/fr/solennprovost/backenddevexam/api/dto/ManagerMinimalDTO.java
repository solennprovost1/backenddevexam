package fr.solennprovost.backenddevexam.api.dto;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@Jacksonized
@SuperBuilder
@Data
public class ManagerMinimalDTO {

    private Long id;
    private String nom;
    private String prenom;

}
