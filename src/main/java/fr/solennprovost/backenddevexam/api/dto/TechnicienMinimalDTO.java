package fr.solennprovost.backenddevexam.api.dto;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

import java.util.Date;

@Jacksonized
@SuperBuilder
@Data
public class TechnicienMinimalDTO {

    private Long id;
    private String nom;
    private String prenom;
    private Date date_de_naissance;

}
