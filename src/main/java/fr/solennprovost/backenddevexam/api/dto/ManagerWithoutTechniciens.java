package fr.solennprovost.backenddevexam.api.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@Jacksonized
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
public class ManagerWithoutTechniciens extends ManagerMinimalDTO {

    private String telephone_fixe;
    private String telephone_portable;

}
