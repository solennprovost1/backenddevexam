package fr.solennprovost.backenddevexam.api.dto;

import fr.solennprovost.backenddevexam.entity.Adresse;
import fr.solennprovost.backenddevexam.entity.Vehicule;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@Jacksonized
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
public class TechnicienWithoutChantiers extends TechnicienMinimalDTO {

    private Adresse adresse;
    private Vehicule vehicule;
    private ManagerMinimalDTO managerMinimalDTO;

}
