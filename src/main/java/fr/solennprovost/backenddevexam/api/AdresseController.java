package fr.solennprovost.backenddevexam.api;


import fr.solennprovost.backenddevexam.entity.Adresse;
import fr.solennprovost.backenddevexam.service.IAdresseService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "/adresse", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class AdresseController {

    private final IAdresseService adresseService;

    @GetMapping
    public ResponseEntity<List<Adresse>> getAll() {
        return new ResponseEntity<>(this.adresseService.getAll(), HttpStatus.OK);
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<Adresse> getById(@PathVariable Long id) {
        if (this.adresseService.getById(id).isPresent()) {
            return ResponseEntity.ok().body(this.adresseService.getById(id).get());
        }
        return null;
    }

    @RequestBody
    @PostMapping
    public ResponseEntity<Adresse> createAdresse(@RequestBody Adresse adresse){
        adresse = this.adresseService.create(adresse);
        return  ResponseEntity.status(HttpStatus.CREATED).body(adresse);
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<Adresse> updateAdresse(@PathVariable Long id, @RequestBody Adresse adresse){
        if (this.adresseService.getById(id).isEmpty()){
            return ResponseEntity.notFound().build();
        }
        if (!Objects.equals(id, adresse.getId())) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(this.adresseService.update(adresse));
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity<Void> deleteAdresse(@PathVariable Long id){
        try {
            this.adresseService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

}
