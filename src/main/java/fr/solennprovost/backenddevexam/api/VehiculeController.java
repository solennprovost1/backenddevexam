package fr.solennprovost.backenddevexam.api;


import fr.solennprovost.backenddevexam.entity.Vehicule;
import fr.solennprovost.backenddevexam.service.IVehiculeService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "/vehicule", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class VehiculeController {

    private final IVehiculeService vehiculeService;

    @GetMapping
    public ResponseEntity<List<Vehicule>> getAll() {
        return new ResponseEntity<>(this.vehiculeService.getAll(), HttpStatus.OK);
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<Vehicule> getById(@PathVariable Long id) {
        if (this.vehiculeService.getById(id).isPresent()) {
            return ResponseEntity.ok().body(this.vehiculeService.getById(id).get());
        }
        return null;
    }

    @RequestBody
    @PostMapping
    public ResponseEntity<Vehicule> createVehicule(@RequestBody Vehicule vehicule){
        vehicule = this.vehiculeService.create(vehicule);
        return  ResponseEntity.status(HttpStatus.CREATED).body(vehicule);
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<Vehicule> updateVehicule(@PathVariable Long id, @RequestBody Vehicule vehicule){
        if (this.vehiculeService.getById(id).isEmpty()){
            return ResponseEntity.notFound().build();
        }
        if (!Objects.equals(id, vehicule.getId())) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(this.vehiculeService.update(vehicule));
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity<Void> deleteVehicule(@PathVariable Long id){
        try {
            this.vehiculeService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
