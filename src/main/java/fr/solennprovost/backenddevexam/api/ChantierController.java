package fr.solennprovost.backenddevexam.api;

import fr.solennprovost.backenddevexam.api.dto.*;
import fr.solennprovost.backenddevexam.entity.Chantier;
import fr.solennprovost.backenddevexam.entity.Technicien;
import fr.solennprovost.backenddevexam.service.IChantierService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/chantier", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class ChantierController {

    private final IChantierService chantierService;

    @GetMapping
    public ResponseEntity<List<ChantierDTO>> getAll() {
        return ResponseEntity.ok(mapToDTOListChantier(
                this.chantierService.getAll()));
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<ChantierDTO> getById(@PathVariable Long id) {
        return this.chantierService.getById(id)
                .map(chantier -> ResponseEntity.ok(mapToDTO(chantier)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @RequestBody
    @PostMapping
    public ResponseEntity<ChantierWithoutTechniciens> createChantier(@RequestBody ChantierWithoutTechniciens chantierWithoutTechniciens){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mapToChantierWithoutTechniciens(this.chantierService
                        .create(mapToEntity(chantierWithoutTechniciens))));

    }

    @PutMapping(path = "{id}")
    public ResponseEntity<ChantierWithoutTechniciens> updateChantier(@PathVariable Long id,
                                                                     @RequestBody ChantierWithoutTechniciens chantierWithoutTechniciens){
        if (this.chantierService.getById(id).isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        if (!Objects.equals(id, chantierWithoutTechniciens.getId())) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(mapToDTO(this.chantierService.update(mapToEntity(chantierWithoutTechniciens))));
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity<Void> deleteChantier(@PathVariable Long id){
        try {
            this.chantierService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }

    }

    private Chantier mapToEntity(ChantierWithoutTechniciens chantierWithoutTechniciens){
        Chantier chantier = new Chantier();
        chantier.setId(chantierWithoutTechniciens.getId());
        chantier.setNom(chantierWithoutTechniciens.getNom());
        chantier.setPrix(chantierWithoutTechniciens.getPrix());
        chantier.setAdresse(chantierWithoutTechniciens.getAdresse());
        return chantier;
    }

    private ChantierWithoutTechniciens mapToChantierWithoutTechniciens(Chantier chantier){
        return ChantierWithoutTechniciens.builder()
                .id(chantier.getId())
                .nom(chantier.getNom())
                .prix(chantier.getPrix())
                .adresse(chantier.getAdresse())
                .build();
    }

    private ChantierDTO mapToDTO(Chantier chantier){
        return ChantierDTO.builder()
                .id(chantier.getId())
                .nom(chantier.getNom())
                .prix(chantier.getPrix())
                .adresse(chantier.getAdresse())
                .technicienMinimalDTOList(mapToMinimalDTOListTechnicien(chantier.getTechniciens()))
                .build();
    }

    private TechnicienMinimalDTO mapToMinimalDTOTechnicien(Technicien technicien) {
        return TechnicienMinimalDTO.builder()
                .id(technicien.getId())
                .nom(technicien.getNom())
                .prenom(technicien.getPrenom())
                .date_de_naissance(technicien.getDate_de_naissance())
                .build();
    }

    List<ChantierDTO> mapToDTOListChantier(List<Chantier> chantiers) {
        return chantiers.stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList());
    }

    List<TechnicienMinimalDTO> mapToMinimalDTOListTechnicien(List<Technicien> techniciens) {
        return techniciens.stream()
                .map(this::mapToMinimalDTOTechnicien)
                .collect(Collectors.toList());
    }
}
