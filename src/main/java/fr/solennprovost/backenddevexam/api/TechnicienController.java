package fr.solennprovost.backenddevexam.api;

import fr.solennprovost.backenddevexam.api.dto.*;
import fr.solennprovost.backenddevexam.entity.Chantier;
import fr.solennprovost.backenddevexam.entity.Manager;
import fr.solennprovost.backenddevexam.entity.Technicien;
import fr.solennprovost.backenddevexam.service.IManagerService;
import fr.solennprovost.backenddevexam.service.ITechnicienService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/technicien", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class TechnicienController {
    private final ITechnicienService technicienService;
    private final IManagerService managerService;

    @GetMapping
    public ResponseEntity<List<TechnicienDTO>> getAll() {
        return ResponseEntity.ok(mapToDTOListTechnicien(
                this.technicienService.getAll()));       }

    @GetMapping(path = "{id}")
    public ResponseEntity<TechnicienDTO> getById(@PathVariable Long id) {
        return this.technicienService.getById(id)
                .map(technicien -> ResponseEntity.ok(mapToDTO(technicien)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @RequestBody
    @PostMapping
    public ResponseEntity<TechnicienWithoutChantiers> createTechnicien(@RequestBody TechnicienWithoutChantiers technicienWithoutChantiers){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mapToTechnicienWithoutChantiers(this.technicienService
                        .create(mapToEntity(technicienWithoutChantiers))));
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<TechnicienWithoutChantiers> updateTechnicien(@PathVariable Long id,
                                                                       @RequestBody TechnicienWithoutChantiers technicienWithoutChantiers){
        if (this.technicienService.getById(id).isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        if (!Objects.equals(id, technicienWithoutChantiers.getId())) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(mapToDTO(this.technicienService.update(mapToEntity(technicienWithoutChantiers))));
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity<Void> deleteTechnicien(@PathVariable Long id){
        try {
            this.technicienService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    private Technicien mapToEntity(TechnicienWithoutChantiers technicienWithoutChantiers){
        Technicien technicien = new Technicien();
        technicien.setId(technicienWithoutChantiers.getId());
        technicien.setNom(technicienWithoutChantiers.getNom());
        technicien.setPrenom(technicienWithoutChantiers.getPrenom());
        technicien.setDate_de_naissance(technicienWithoutChantiers.getDate_de_naissance());
        technicien.setAdresse(technicienWithoutChantiers.getAdresse());
        technicien.setVehicule(technicienWithoutChantiers.getVehicule());
        technicien.setManager(getManagerToDTOWithoutChantiers(technicienWithoutChantiers.getManagerMinimalDTO()));
        return technicien;
    }

    private Manager getManagerToDTOWithoutChantiers(ManagerMinimalDTO managerMinimalDTO){
        Long id = managerMinimalDTO.getId();
        return managerService.getById(id).orElseThrow();
    }

    private TechnicienWithoutChantiers mapToTechnicienWithoutChantiers(Technicien technicien){
        return TechnicienWithoutChantiers.builder()
                .id(technicien.getId())
                .nom(technicien.getNom())
                .prenom(technicien.getPrenom())
                .date_de_naissance(technicien.getDate_de_naissance())
                .vehicule(technicien.getVehicule())
                .adresse(technicien.getAdresse())
                .managerMinimalDTO(mapToManagerMinimalDTO(technicien.getManager()))
                .build();
    }

    private TechnicienDTO mapToDTO(Technicien technicien){
        return TechnicienDTO.builder()
                .id(technicien.getId())
                .nom(technicien.getNom())
                .prenom(technicien.getPrenom())
                .date_de_naissance(technicien.getDate_de_naissance())
                .adresse(technicien.getAdresse())
                .vehicule(technicien.getVehicule())
                .managerMinimalDTO(mapToManagerMinimalDTO(technicien.getManager()))
                .chantierMinimalDTOList(mapToDTOListChantierMinimal(technicien.getChantiers()))
                .build();
    }

    private ManagerMinimalDTO mapToManagerMinimalDTO(Manager manager){
        return ManagerMinimalDTO.builder()
                .id(manager.getId())
                .nom(manager.getNom())
                .prenom(manager.getPrenom())
                .build();
    }
    private ChantierMinimalDTO mapToChantierMinimalDTO(Chantier chantier){
        return ChantierMinimalDTO.builder()
                .id(chantier.getId())
                .nom(chantier.getNom())
                .prix(chantier.getPrix())
                .build();
    }

    List<TechnicienDTO> mapToDTOListTechnicien(List<Technicien> techniciens){
        return techniciens.stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList());
    }

    List<ChantierMinimalDTO> mapToDTOListChantierMinimal(List<Chantier> chantiers) {
        return chantiers.stream()
                .map(this::mapToChantierMinimalDTO)
                .collect(Collectors.toList());
    }



}
