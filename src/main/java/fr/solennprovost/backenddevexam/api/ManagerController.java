package fr.solennprovost.backenddevexam.api;

import fr.solennprovost.backenddevexam.api.dto.ManagerDTO;
import fr.solennprovost.backenddevexam.api.dto.ManagerWithoutTechniciens;
import fr.solennprovost.backenddevexam.api.dto.TechnicienMinimalDTO;
import fr.solennprovost.backenddevexam.entity.Manager;
import fr.solennprovost.backenddevexam.entity.Technicien;
import fr.solennprovost.backenddevexam.service.IManagerService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/manager", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class ManagerController {

    private final IManagerService managerService;

    @GetMapping
    public ResponseEntity<List<ManagerDTO>> getAll() {
        return ResponseEntity.ok(mapToDTOListManager(
                this.managerService.getAll()));
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<ManagerDTO> getById(@PathVariable Long id) {
        return this.managerService.getById(id)
                .map(manager -> ResponseEntity.ok(mapToDTO(manager)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<ManagerWithoutTechniciens> createManager(@RequestBody ManagerWithoutTechniciens managerWithoutTechniciens) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mapToManagerWithoutTechniciens(this.managerService
                        .create(mapToEntity(managerWithoutTechniciens))));
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<ManagerWithoutTechniciens> updateManager(@PathVariable Long id,
                                                                   @RequestBody ManagerWithoutTechniciens managerWithoutTechniciens){
        if (this.managerService.getById(id).isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        if (!Objects.equals(id, managerWithoutTechniciens.getId())) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(mapToDTO(this.managerService.update(mapToEntity(managerWithoutTechniciens))));
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity<Void> deleteManager(@PathVariable Long id) {
        try {
            this.managerService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    private Manager mapToEntity(ManagerWithoutTechniciens managerWithoutTechniciens){
        Manager manager = new Manager();
        manager.setId(managerWithoutTechniciens.getId());
        manager.setNom(managerWithoutTechniciens.getNom());
        manager.setPrenom(managerWithoutTechniciens.getPrenom());
        manager.setTelephone_fixe(managerWithoutTechniciens.getTelephone_fixe());
        manager.setTelephone_portable(managerWithoutTechniciens.getTelephone_portable());
        return manager;
    }

    private ManagerWithoutTechniciens mapToManagerWithoutTechniciens(Manager manager){
        return ManagerWithoutTechniciens.builder()
                .id(manager.getId())
                .nom(manager.getNom())
                .prenom(manager.getPrenom())
                .telephone_fixe(manager.getTelephone_fixe())
                .telephone_portable(manager.getTelephone_portable())
                .build();
    }

    private ManagerDTO mapToDTO(Manager manager){
        return ManagerDTO.builder()
                .id(manager.getId())
                .nom(manager.getNom())
                .prenom(manager.getPrenom())
                .telephone_fixe(manager.getTelephone_fixe())
                .telephone_portable(manager.getTelephone_portable())
                .technicienMinimalDTOList(mapToMinimalDTOListTechnicien(manager.getList_technicien()))
                .build();
    }

    private TechnicienMinimalDTO mapToMinimalDTOTechnicien(Technicien technicien) {
        return TechnicienMinimalDTO.builder()
                .id(technicien.getId())
                .nom(technicien.getNom())
                .prenom(technicien.getPrenom())
                .date_de_naissance(technicien.getDate_de_naissance())
                .build();
    }

    List<ManagerDTO> mapToDTOListManager(List<Manager> managers) {
        return managers.stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList());
    }

    List<TechnicienMinimalDTO> mapToMinimalDTOListTechnicien(List<Technicien> techniciens) {
        return techniciens.stream()
                .map(this::mapToMinimalDTOTechnicien)
                .collect(Collectors.toList());
    }



}

