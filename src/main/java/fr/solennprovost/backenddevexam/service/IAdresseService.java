package fr.solennprovost.backenddevexam.service;

import fr.solennprovost.backenddevexam.entity.Adresse;

import java.util.List;
import java.util.Optional;

public interface IAdresseService {

    List<Adresse> getAll();
    Optional<Adresse> getById(Long id);
    Adresse create(Adresse adresse);
    Adresse update(Adresse adresse);
    void delete(Long id);
}
