package fr.solennprovost.backenddevexam.service.impl;

import fr.solennprovost.backenddevexam.entity.Chantier;
import fr.solennprovost.backenddevexam.repository.ChantierRepository;
import fr.solennprovost.backenddevexam.service.IChantierService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ChantierServiceImpl implements IChantierService {

    private final ChantierRepository chantierRepository;

    @Override
    public List<Chantier> getAll() { return this.chantierRepository.findAll();}

    @Override
    public Optional<Chantier> getById(Long id){ return this.chantierRepository.findById(id);}

    @Override
    public Chantier create(Chantier chantier) {
        chantier.setId(null);
        return this.chantierRepository.save(chantier);
    }

    @Override
    public Chantier update(Chantier chantier) {
        return this.chantierRepository.save(chantier);
    }

    @Override
    public void delete(Long id) throws EntityNotFoundException {
        Optional<Chantier> optionalChantier = this.chantierRepository.findById(id);
        if (optionalChantier.isPresent()) {
            this.chantierRepository.delete(optionalChantier.get());
        } else {
            throw new EntityNotFoundException();
        }
    }
}
