package fr.solennprovost.backenddevexam.service.impl;

import fr.solennprovost.backenddevexam.entity.Technicien;
import fr.solennprovost.backenddevexam.repository.TechnicienRepository;
import fr.solennprovost.backenddevexam.service.ITechnicienService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class TechnicienServiceImpl implements ITechnicienService {

    private final TechnicienRepository technicienRepository;

    @Override
    public List<Technicien> getAll() { return this.technicienRepository.findAll();}

    @Override
    public Optional<Technicien> getById(Long id){ return this.technicienRepository.findById(id);}

    @Override
    public Technicien create(Technicien technicien) {
        technicien.setId(null);
        return this.technicienRepository.save(technicien);
    }

    @Override
    public Technicien update(Technicien technicien) {
        return this.technicienRepository.save(technicien);
    }

    @Override
    public void delete(Long id) throws EntityNotFoundException {
        Optional<Technicien> optionalTechnicien = this.technicienRepository.findById(id);
        if (optionalTechnicien.isPresent()) {
            this.technicienRepository.delete(optionalTechnicien.get());
        } else {
            throw new EntityNotFoundException();
        }
    }
}
