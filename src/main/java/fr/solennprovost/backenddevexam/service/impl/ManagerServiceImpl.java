package fr.solennprovost.backenddevexam.service.impl;

import fr.solennprovost.backenddevexam.entity.Manager;
import fr.solennprovost.backenddevexam.repository.ManagerRepository;
import fr.solennprovost.backenddevexam.service.IManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ManagerServiceImpl implements IManagerService {

    private final ManagerRepository managerRepository;

    @Override
    public List<Manager> getAll() { return this.managerRepository.findAll();}

    @Override
    public Optional<Manager> getById(Long id){ return this.managerRepository.findById(id);}

    @Override
    public Manager create(Manager manager) {
        manager.setId(null);
        return this.managerRepository.save(manager);
    }

    @Override
    public Manager update(Manager manager) {
        return this.managerRepository.save(manager);
    }

    @Override
    public void delete(Long id) throws EntityNotFoundException {
        Optional<Manager> optionalManager = this.managerRepository.findById(id);
        if (optionalManager.isPresent()) {
            this.managerRepository.delete(optionalManager.get());
        } else {
            throw new EntityNotFoundException();
        }
    }
}
