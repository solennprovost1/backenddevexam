package fr.solennprovost.backenddevexam.service.impl;

import fr.solennprovost.backenddevexam.entity.Vehicule;
import fr.solennprovost.backenddevexam.repository.VehiculeRepository;
import fr.solennprovost.backenddevexam.service.IVehiculeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class VehiculeServiceImpl implements IVehiculeService {

    private final VehiculeRepository vehiculeRepository;

    @Override
    public List<Vehicule> getAll() { return this.vehiculeRepository.findAll();}

    @Override
    public Optional<Vehicule> getById(Long id){ return this.vehiculeRepository.findById(id);}

    @Override
    public Vehicule create(Vehicule vehicule) {
        return this.vehiculeRepository.save(vehicule);
    }

    @Override
    public Vehicule update(Vehicule vehicule) {
        return this.vehiculeRepository.save(vehicule);
    }

    @Override
    public void delete(Long id) throws EntityNotFoundException {
        Optional<Vehicule> optionalVehicule = this.vehiculeRepository.findById(id);
        if (optionalVehicule.isPresent()) {
            this.vehiculeRepository.delete(optionalVehicule.get());
        } else {
            throw new EntityNotFoundException();
        }
    }
}
