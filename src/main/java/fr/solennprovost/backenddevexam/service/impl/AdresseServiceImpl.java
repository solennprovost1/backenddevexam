package fr.solennprovost.backenddevexam.service.impl;

import fr.solennprovost.backenddevexam.entity.Adresse;
import fr.solennprovost.backenddevexam.repository.AdresseRepository;
import fr.solennprovost.backenddevexam.service.IAdresseService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class AdresseServiceImpl implements IAdresseService {

    private final AdresseRepository adresseRepository;

    @Override
    public List<Adresse> getAll() { return this.adresseRepository.findAll();}

    @Override
    public Optional<Adresse> getById(Long id){ return this.adresseRepository.findById(id);}

    @Override
    public Adresse create(Adresse adresse) {
        adresse.setId(null);
        return this.adresseRepository.save(adresse);
    }

    @Override
    public Adresse update(Adresse adresse) {
        return this.adresseRepository.save(adresse);
    }

    @Override
    public void delete(Long id) throws EntityNotFoundException {
        Optional<Adresse> optionalAdresse = this.adresseRepository.findById(id);
        if (optionalAdresse.isPresent()) {
            this.adresseRepository.delete(optionalAdresse.get());
        } else {
            throw new EntityNotFoundException();
        }
    }
}
