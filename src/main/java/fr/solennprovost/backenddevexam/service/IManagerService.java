package fr.solennprovost.backenddevexam.service;

import fr.solennprovost.backenddevexam.entity.Manager;

import java.util.List;
import java.util.Optional;

public interface IManagerService {

    List<Manager> getAll();
    Optional<Manager> getById(Long id);
    Manager create(Manager manager);
    Manager update(Manager manager);
    void delete(Long id);
}
