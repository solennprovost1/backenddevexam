package fr.solennprovost.backenddevexam.service;

import fr.solennprovost.backenddevexam.entity.Adresse;
import fr.solennprovost.backenddevexam.entity.Technicien;

import java.util.List;
import java.util.Optional;

public interface ITechnicienService {

    List<Technicien> getAll();
    Optional<Technicien> getById(Long id);
    Technicien create(Technicien technicien);
    Technicien update(Technicien technicien);
    void delete(Long id);

}
