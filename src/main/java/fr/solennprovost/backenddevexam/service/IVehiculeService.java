package fr.solennprovost.backenddevexam.service;

import fr.solennprovost.backenddevexam.entity.Vehicule;

import java.util.List;
import java.util.Optional;

public interface IVehiculeService {

    List<Vehicule> getAll();
    Optional<Vehicule> getById(Long id);
    Vehicule create(Vehicule vehicule);
    Vehicule update(Vehicule vehicule);
    void delete(Long id);

}
