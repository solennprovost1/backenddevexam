package fr.solennprovost.backenddevexam.service;

import fr.solennprovost.backenddevexam.entity.Chantier;

import java.util.List;
import java.util.Optional;

public interface IChantierService {

    List<Chantier> getAll();
    Optional<Chantier> getById(Long id);
    Chantier create(Chantier chantier);
    Chantier update(Chantier chantier);
    void delete(Long id);
}
