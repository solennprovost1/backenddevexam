package fr.solennprovost.backenddevexam.config;

import org.springframework.http.HttpMethod;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("technicien").password(this.passwordEncoder.encode("technicien")).roles("TECHNICIEN")
                .and()
                .withUser("manager").password(this.passwordEncoder.encode("manager")).roles("MANAGER")
                .and()
                .withUser("rh").password(this.passwordEncoder.encode("rh")).roles("RH")
                .and()
                .withUser("admin").password(this.passwordEncoder.encode("admin")).roles("ADMIN");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .httpBasic().and()
                .formLogin().and()
                .logout().deleteCookies("JSESSIONID").invalidateHttpSession(true).and()
                .authorizeRequests().antMatchers("/login", "/swagger-resources/**", "/swagger-ui/**").permitAll()
                .antMatchers(HttpMethod.GET, "/technicien/{id}").hasRole("TECHNICIEN")
//                .antMatchers(HttpMethod.GET, "/technicien/{id}/chantier").hasRole("TECHNICIEN")
                .antMatchers(HttpMethod.GET, "/chantier").hasRole("MANAGER")
                .antMatchers(HttpMethod.POST, "/chantier").hasRole("MANAGER")
                .antMatchers(HttpMethod.DELETE, "/chantier").hasRole("MANAGER")
//                .antMatchers(HttpMethod.PUT, "/chantier/{id}/technicien").hasRole("MANAGER")
                .antMatchers(HttpMethod.POST, "/manager").hasRole("RH")
                .antMatchers(HttpMethod.DELETE, "/manager").hasRole("RH")
                .antMatchers(HttpMethod.POST, "/technicien").hasRole("RH")
                .antMatchers(HttpMethod.DELETE, "/technicien").hasRole("RH")
                .antMatchers(HttpMethod.PUT, "/technicien").hasRole("RH")
                .anyRequest().hasRole("ADMIN");
    }

}
