package fr.solennprovost.backenddevexam.repository;

import fr.solennprovost.backenddevexam.entity.Technicien;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TechnicienRepository extends JpaRepository<Technicien, Long> {
}
