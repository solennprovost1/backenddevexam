package fr.solennprovost.backenddevexam.repository;

import fr.solennprovost.backenddevexam.entity.Adresse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdresseRepository extends JpaRepository<Adresse, Long> {
}
