package fr.solennprovost.backenddevexam.repository;

import fr.solennprovost.backenddevexam.entity.Chantier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChantierRepository extends JpaRepository<Chantier, Long> {
}
