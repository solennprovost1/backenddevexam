package fr.solennprovost.backenddevexam.repository;

import fr.solennprovost.backenddevexam.entity.Manager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManagerRepository extends JpaRepository<Manager, Long> {
}
