package fr.solennprovost.backenddevexam.repository;

import fr.solennprovost.backenddevexam.entity.Vehicule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehiculeRepository extends JpaRepository<Vehicule, Long> {
}
