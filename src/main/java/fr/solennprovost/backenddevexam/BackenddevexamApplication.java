package fr.solennprovost.backenddevexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackenddevexamApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackenddevexamApplication.class, args);
	}

}
